#!/bin/bash
trinet_url="haymoralocal.com"
box_IP="192.168.1.11"

rpm -ivh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm

yum install nano ansible vi git -y

cp /vagrant/bashrc /home/vagrant/.bash_profile
cp -R /vagrant/.nano/ /home/vagrant/.nano/
cp /home/vagrant/.nano/.nanorc /home/vagrant/.nanorc

echo -e "127.0.0.1	webserver	webserver
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
" > /tmp/hosts_file

cp /tmp/hosts_file /etc/hosts
