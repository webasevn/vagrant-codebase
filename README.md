# README #

Vagrant codebase
(Download MacDown ro view this README.md)

### What is this repository for? ###

* This repo contains codebase for `Vagrant environment`
* Version: 1.0.0
* [CodeBase on Bitbucket](https://cachepdo@bitbucket.org/cachepdo/vagrant-codebase.git)

### How do I get set up development environment with Vagrant? ###
#### Install Vagrant/VirtualBox & then run command
    $ vagrant up
    
After vagrant has been built, ssh to Vagrant:

    $ vagrant ssh
    $ sudo su

#### Install PHP
    $ yum update -y
    $ yum install -y yum-utils
    $ yum install -y epel-release
    $ wget https://rpms.remirepo.net/enterprise/remi-release-7.rpm
    $ rpm -ivh remi-release-7.rpm
    $ yum update
    $ yum install -y php56 php56-php php56-php-mysqlnd php56-php-gd php56-php-mcrypt php56-php-mbstring php56-php-xml php56-php-cli
    
#### Install Webserver
    $ yum install -y httpd
    $ systemctl start httpd
    $ ystemctl enable httpd
    $ systemctl status httpd
    
#### Install SSL
    $ yum install -y mod_ssl
    $ nano /etc/httpd/conf.d/ssl.conf
    
    # Use name-base virtual hosting
    # NameVirtualHost *:443
    ...
    ### Uncomment these lines
    DocumentRoot "/var/www/example.com/public_html" # change path to webroot
    ServerName www.example.com:443 # change domain name
    ...
    ### Comment these lines
    # SSLProtocol all -SSLv2
    # SSLCipherSuite HIGH:MEDIUM:!aNULL:!MD5:!SEED:!IDEA

#### Enable HTTP/HTTPS to Firewall
    $ firewall-cmd --zone=public --permanent --add-service=http
    $ firewall-cmd --zone=public --permanent --add-service=https
    $ firewall-cmd --reload
    $ systemctl restart httpd.service
    $ apachectl configtest
    
#### INSTALL MariaDB 5.5
    $ nano /etc/yum.repos.d/mariadb.repo
    
#####  Cấu hình repo cho MariaDB
###### For CentOS/RHEL 7 (64 Bit)
    [mariadb]
    name = MariaDB
    baseurl = http://yum.mariadb.org/5.5/rhel7-amd64/
    gpgkey=https://yum.mariadb.org/RPM-GPG-KEY-MariaDB
    gpgcheck=1
    
###### For CentOS/RHEL 6 (64 Bit)
    [mariadb]
    name = MariaDB
    baseurl = http://yum.mariadb.org/5.5/rhel6-amd64
    gpgkey=https://yum.mariadb.org/RPM-GPG-KEY-MariaDB
    gpgcheck=1
   
###### Install
    $ yum install -y MariaDB-server MariaDB-client
    $ service mysql start
    $ mysql_secure_installation
