alias ll='ls -al'
alias l='ls -al'
alias up='vagrant up'
alias down='vagrant destroy -f'
alias ip='curl icanhazip.com'
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."
alias vg='vagrant global-status'
alias vt='down && up'
alias ghis='history | grep'
alias reload='source ~/.bash_profile'
alias mod='nano ~/.bash_profile'
alias grep='grep --color'

export CLICOLOR=1
export LSCOLORS=GxFxCxDxBxegedabagaced

# Prompt
BGREEN='\[\033[1;32m\]'
GREEN='\[\033[0;32m\]'
BRED='\[\033[1;31m\]'
RED='\[\033[0;31m\]'
BBLUE='\[\033[1;34m\]'
BLUE='\[\033[0;34m\]'
NORMAL='\[\033[00m\]'
PS1="${BGREEN}Vagrant ${BLUE}(${RED}\w${BLUE}) ${GREEN}\@ ${BBLUE}\$ ${NORMAL}"

export EDITOR='subl -w'
export HISTFILESIZE=20000
export HISTSIZE=10000
export HISTTIMEFORMAT="%d/%m/%y %T "
shopt -s histappend
# Combine multiline commands into one in history
shopt -s cmdhist
# Ignore duplicates, ls without options and builtin commands
HISTCONTROL=ignoredups
export HISTIGNORE="&:ls:[bf]g:exit"