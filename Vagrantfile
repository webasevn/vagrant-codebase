# -*- mode: ruby -*-
# vi: set ft=ruby :

require 'yaml'

code_root = "~/Sites/"

dir = File.dirname(File.expand_path(__FILE__))

Vagrant.require_version ">= 1.7.3", "!=2.1.3", "!=2.1.4"

require_plugins = [
	{ :name => "vagrant-hostmanager", :version => ">=1.8.0" },
	{ :name => "vagrant-vbguest", :version => ">=0.12.0" }
]

exit unless require_plugins.all? do |plugin|
  Vagrant.has_plugin?(plugin[:name], plugin[:version]) || (
    raise Vagrant::Errors::VagrantError.new,
	"Plugin #{plugin[:name]}, version #{plugin[:version]}, is required. Please install or update it with:\n" +
	"$ vagrant plugin install #{plugin[:name]}"
  )
end

VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
	config.vm.synced_folder ".", "/vagrant"
	config.vm.synced_folder "./.key", "/vagrant/.key",
		owner: "vagrant", group: "vagrant", :mount_options => ['dmode=755', 'fmode=400']

	config.ssh.insert_key = false

	config.vm.provision "shell", inline: "[ ! -e /vagrant/.key/vagrant_key ] && [ ! -e /vagrant/.key/vagrant_key.pub ] && ssh-keygen -t rsa -f /vagrant/.key/vagrant_key -q -P ''; echo \"$(cat /home/vagrant/.ssh/authorized_keys)\" > /home/vagrant/.ssh/authorized_keys; cat /vagrant/.key/vagrant_key.pub >> /home/vagrant/.ssh/authorized_keys"

	config.vm.synced_folder "#{code_root}{REPLACE_YOUR_PROJECT_NAME}", "/var/www/{YOUR_PROJECT_NAME}", owner: "vagrant", group: "vagrant"

	config.hostmanager.enabled = true
	config.hostmanager.manage_host = true
	config.hostmanager.ignore_private_ip = false
	config.hostmanager.include_offline = true

	config.vm.provider :virtualbox do |v|
		v.customize [ "guestproperty", "set", :id, "/VirtualBox/GuestAdd/VBoxService/--timesync-set-threshold", 10000 ]
		v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
		v.customize ["modifyvm", :id, "--memory", 1024]
		v.customize ["modifyvm", :id, "--cpus", 1]
		v.customize ["modifyvm", :id, "--ioapic", "on"]
	end
	config.vm.define "webserver" do |web|
		web.vm.hostname = "webserver"
		web.vm.box = "elastic/centos-7-x86_64"
		web.vm.network :private_network, ip: "192.168.1.11"
		web.hostmanager.aliases = %w(domain.comlocal)
		web.vm.provision "shell", path: "scripts/start.sh"
		web.vm.provision "shell", inline: "echo '192.168.1.11 domain.comlocal' >> /etc/hosts"
	end
end
